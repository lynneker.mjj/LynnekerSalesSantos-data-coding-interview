from typing import Optional

from pydantic import BaseModel

from flask_openapi3 import APIBlueprint
from sqlalchemy import select

from database import db
from api.users.models import Users
from flask import request

from flask import jsonify
from confluent_kafka import Producer

users_app = APIBlueprint("users_app", __name__)

p = Producer({'bootstrap.servers': 'localhost:9092'})

class UserSchema(BaseModel):
    id: int
    password: str
    email: str
    created_at: str
    updated_at: str
    last_login: str
    first_name: str
    last_name: str


class UserList(BaseModel):
    users: list[UserSchema]


@users_app.get("/users", responses={"200": UserList})
def get_users():
    with db.session() as session:
        users = session.execute(select(Users)).scalars().all()
        _users = jsonify(users)
        p.produce('TOPIC', _users.data)
        return _users


@users_app.post("/users")
def create_user():
    user = Users(**request.form)
    with db.session() as session:
        session.add(user)
        session.commit()
        return '',201
        
