import uuid

def test_get_users(client):
    resp = client.get('/users')
    assert resp.status_code == 200
    assert resp.content_length > 0
    assert type(resp.get_json()) == list
    assert b'email' in resp.data

def test_create_users(client):
    user = dict(
        password="test",
        email=f"{uuid.uuid4().hex}@email.com",
        created_at="2021-01-01",
        updated_at="2021-01-01",
        first_name="test",
        last_name="test",
    )
    resp = client.post('/users', data=user)
    assert resp.status_code == 201
    assert resp.content_length == 0